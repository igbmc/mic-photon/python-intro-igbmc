---
title: "Introduction to Python programming"
subtitle: "**For researchers @ IGBMC**"
author: "Marco Dalla Vecchia"
institute: IGBMC
date: 03/05/2023
format: 
    revealjs:
        theme: sky
        self-contained: false
        slide-number: true
        chalkboard: true
        logo: "assets/logo.png"
        toc: true
        toc-depth: 1
        center: true
        highlight-style: a11y
jupyter: python3
---

# Practicalities

## Before we start...
1. Connect to [https://jupyterhub.igbmc.fr/](https://jupyterhub.igbmc.fr/)
2. Select the `python-intro` reservation
3. Leave the other settings to the default values and press `Start`
4. Start a terminal window and copy/paste this command:

```
git clone https://gitlab.com/igbmc/mic-photon/python-intro-igbmc.git
```

5. Let me or Jean-Christophe if something goes wrong
6. Place the post-it on your laptop if you are done!

## Course Schedule
- DAY1: **03/05/2023** &rarr; room **E1031 CBI**
- DAY2: **10/05/2023** &rarr; room **3013 ICS**

### Time: 
- Morning: 9.30am - 12.00am
- Afternoon: 1.30pm - 5.00pm

## Questionnaire results - Schedule

![](assets/schedule-results.png)

## Questionnaire results - Experience

![](assets/previous-programming-experience-results.png)

## Questionnaire results - Exercises

![](assets/exercises-results.png)

## Course Material and Contacts
- Slides / Notebooks / Exercise &rarr; [gitlab repository](https://gitlab.com/igbmc/mic-photon/python-intro-igbmc)
- Contact &rarr; send me an email at [dallavem@igbmc.fr](mailto:dallavem@igbmc.fr)
- JupyterHub issues &rarr; contact IT [helpdesk@igbmc.fr](mailto:helpdesk@igbmc.fr)

## Questionnaire results - Goals

![](assets/phase-time-consumption-results.png)

## Hands-on project overview
![](assets/project_overview.png)

# Why Python?

::::{.columns}
:::{.column width="70%" .incremental}
1. Easy to learn!
2. Huge active collaborative community
3. Extremely flexible
4. Large variety of packages for nearly any application!
5. The present and future for artifical intelligence applications
:::

:::{.column width="30%"}
![](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Python-logo-notext.svg/1869px-Python-logo-notext.svg.png)
:::
::::

## What can you do with Python? {.smaller}

More like what you can **NOT** do with Python!

![[Source](https://www.javatpoint.com/python-applications)](https://static.javatpoint.com/python/images/python-applications.png)

## Coding or Programming?

- **Coding**: _writing codes that a machine can understand_
- **Programming**: _creating programs that involve the ratification of codes_

## What will we do today?

::::{.columns}
:::{.column width="60%" .incremental}
- We will learn the basics of _coding_ in Python
- To write Python _instructions_ that the computer can understand
- You can then create _programs_ that do what you desire yourself!
:::

:::{.column width="40%"}
```{python}
#| label: fig-polar
#| fig-cap: "Coding vs Programming"
import matplotlib.pyplot as plt

f, ax = plt.subplots(1,1)
circle1 = plt.Circle((0.5, 0.5), 0.5, color='tab:orange', alpha=0.5)
circle2 = plt.Circle((0.6, 0.6), 0.2, color='tab:green', alpha=0.5)
ax.set_aspect(1)
ax.add_artist(circle1)
ax.add_artist(circle2)

ax.text(0.6,0.6, 'Coding', va='center', ha='center', fontsize=18, fontname = 'monospace')
ax.text(0.35,0.3, 'Programming', va='center', ha='center', fontsize=18, fontname = 'monospace')
plt.axis('off')
plt.show()
```
:::
::::

# Python Environment

![](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Python_logo_and_wordmark.svg/2560px-Python_logo_and_wordmark.svg.png)

## Python installation

::::{.columns}
:::{.column width="50%" .fragment}
### Python website
- [Python.org](https://www.python.org/) download for most platforms
- On MacOS and Linux Python is often pre-installed
- Pure python is not very useful, we often need extra packages
:::

:::{.column width="50%" .fragment}
### Anaconda
- [Anaconda](https://www.anaconda.com/) is a Python and R distribution to simplify package management and deployment
- Allows for more versatility and reproducibility
- Strongly recommended even from the start!
:::
::::

## Integrated Development Environments (IDE)
Coding ecosystem of tools that help you **editing, compiling, testing** and **using** Python code.

There are many available:

- [PyCharm](https://www.jetbrains.com/pycharm/)
- [Spyder](https://www.spyder-ide.org/) (included with Anaconda)
- [Visual Studio Code](https://code.visualstudio.com/)
- ...

## Easiest way to start (at home)
1. Follow Anaconda installation instructions [here](https://docs.anaconda.com/free/anaconda/install/index.html)
2. Open Anaconda navigator
3. Start up Spyder or a Jupyter notebook
4. Create a new script / notebook
5. Start coding!

# Jupyter Project

![[Source](https://fr.wikipedia.org/wiki/Jupyter)](https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/1200px-Jupyter_logo.svg.png){width=30%}

## Jupyter Lab {.smaller}

> _Web-based interactive development environment for Python and much more_

![](assets/jupyter-lab-screenshot.png)

## Jupyter Hub

> _A multi-user version of the notebook designed for companies, classrooms and research labs_

- Deployed from IGBMC IT service on our local HPC
- Try it yourself and connect to [jupyterhub.igbmc.fr](https://jupyterhub.igbmc.fr/)
- No local installation on your computer required!

# Python Basics

_Time to get started!_

## Objects and Variables

- **Objects** are boxes with certain properties which contain information
- **Variables** are _nicknames_ used to refer to an object

## Objects and Variables

```{python}
#| echo: true
x = 10
```

1. We created an **object** of type `int` (_integer_)
2. We gave it a **value** of 10
3. And associated it with the **variable** of name `x`

## Objects and Variables

```{python}
#| echo: true
word = "potato"
```

1. We created an **object** of type `str` (_string_)
2. We gave it a value of `potato`
3. And associated it with the **variable** of name `word`

## Objects and Variables

```{python}
#| echo: true

x = 10
type(x)
```

- `10` is the **value**
- `x` is the **variable**
- the **object** is hidden of type `int` (_integer_)

## Objects and Variables

```{python}
#| echo: true

word = "potato"
type(word)
```

- `potato` is the **value**
- `word` is the **variable**
- the **object** is hidden of type `str` (_string_)

## Variable names

```{python}
#| echo: true
x = 1
word = "potato"
mY_crazy_VARiable = 100
```

- Cannot start with a number
- Cannot contain special characters (only underscores)
- Is case-sensitive
- Should not be already taken (pre-defined variables or functions)
- Can be anything we want

## Objects

- There are MANY types of objects, we will some of the most common ones
- Objects have properties defined by their **Class**:
    - Object variable &rarr; **attribute**
    - Object function &rarr; **method**
    
## Objects {auto-animate=true}

```{python}
class Student():
    def __init__(self, name, age):
        self.name = name
        self.age = age
    def presentation(self):
        return "Hello! My name is {}".format(self.name.title())
```

```{python}
#| echo: true
me = Student('marco', 30)
```

`me` is the **variable** associated with a `Student` **object**

## Objects {auto-animate=true}

```{python}
#| echo: true
me = Student('marco', 30)
me.name
```

`name` is an attribute of the `Student` object

we can access an **attribute** with a dot (`.`)

## Objects {auto-animate=true}

```{python}
#| echo: true
me = Student('marco', 30)
me.name
me.age
```
`age` is another attribute of the `Student` object

## Objects {auto-animate=true}

```{python}
#| echo: true
me = Student('marco', 30)
me.name
me.age
me.presentation()
```

`presentation` is a **method** of the `Student` object

Like any function, we can call a method using parenthesis `()`

We will see functions later in the course

## Comments

- Any line with a hashtag `#` before it is a **comment**.

```python
# this is a comment!
x = 10 # this is comment to say that x is a number
```

- They useful to leave documentation or explanation of the code
- They can be used to _inactivate_ a line of code to help testing or finding errors

## Strings {auto-animate=true}

```{python}
#| echo: true
str1 = 'potatoes'
str2 = 'are delicious'
str3 = "STRINGS CAN BE DEFINED WITH DOUBLE QUOTES"
str4 = 'they can contain special characters: !@#$%^&*()'
```

## Strings {auto-animate=true}

```{python}
#| echo: true
str1 = 'potatoes'
str2 = 'are delicious'
str3 = "STRINGS CAN BE DEFINED WITH DOUBLE QUOTES"
str4 = 'they can contain special characters: !@#$%^&*()'

# Concatenation
str1 + " " + str2
```

## Strings {auto-animate=true}

```{python}
#| echo: true
str1 = 'potatoes'
str2 = 'are delicious'
str3 = "STRINGS CAN BE DEFINED WITH DOUBLE QUOTES"
str4 = 'they can contain special characters: !@#$%^&*()'

# Slicing
str4[17:24] # from : to
```

## Numerical variables {auto-animate=true}

```{python}
#| echo: true

var1 = 500 # int
var2 = 1.5 # float
var3 = 2e8 # float
var4 = 100_000 # you can use underscore to separate decimals
```

## Numerical variables {auto-animate=true}

```{python}
#| echo: true

var1 = 500 # int
var2 = 1.5 # float
var3 = 2e8 # float
var4 = 100_000 # you can use underscore to separate decimals

# Numerical operations
var1 ** 2
```

## Numerical variables {auto-animate=true}

```{python}
#| echo: true

var1 = 500 # int
var2 = 1.5 # float
var3 = 2e8 # float
var4 = 100_000 # you can use underscore to separate decimals

# Numerical operations
var1 * var2
```

## Numerical variables {auto-animate=true}

```{python}
#| echo: true

var1 = 500 # int
var2 = 1.5 # float
var3 = 2e8 # float
var4 = 100_000 # you can use underscore to separate decimals

# Numerical operations
var4 - var1
```

## Numerical variables {auto-animate=true}

```{python}
#| echo: true

var1 = 500 # int
var2 = 1.5 # float
var3 = 2e8 # float
var4 = 100_000 # you can use underscore to separate decimals

# Numerical operations
var4 / var1
```

## Numerical variables {auto-animate=true}

```{python}
#| echo: true

var1 = 500 # int
var2 = 1.5 # float
var3 = 2e8 # float
var4 = 100_000 # you can use underscore to separate decimals

# Numerical operations
var1 % 2
```

## Numerical variables {auto-animate=true}

```{python}
#| echo: true

var1 = 500 # int
var2 = 1.5 # float
var3 = 2e8 # float
var4 = 100_000 # you can use underscore to separate decimals

# Comparison
var3 < var1
```

## Numerical variables {auto-animate=true}

```{python}
#| echo: true

var1 = 500 # int
var2 = 1.5 # float
var3 = 2e8 # float
var4 = 100_000 # you can use underscore to separate decimals

# Comparison
var4 == 1e5
```

## Lists {auto-animate=true}

Lists are **ordered** and **mutable** data containers

```{python}
#| echo: true
l1 = [] # empty list
l2 = [1,2,3] # list of numbers
l3 = ['this','is','a','list'] # list of strings
l4 = ['WOW', 500, 3.4, l2] # list of anything!
```

## Lists {auto-animate=true}

```{python}
#| echo: true
l1 = [] # empty list
l2 = [1,2,3] # list of numbers
l3 = ['this','is','a','list'] # list of strings
l4 = ['WOW', 500, 3.4, l2] # list of anything!

# Concatenation

l2 + l2
```

## Lists {auto-animate=true}

```{python}
#| echo: true
l1 = [] # empty list
l2 = [1,2,3] # list of numbers
l3 = ['this','is','a','list'] # list of strings
l4 = ['WOW', 500, 3.4, l2] # list of anything!

# Slicing
l4[1:-1]
```

## Lists {auto-animate=true}

```{python}
#| echo: true
l1 = [] # empty list
l2 = [1,2,3] # list of numbers
l3 = ['this','is','a','list'] # list of strings
l4 = ['WOW', 500, 3.4, l2] # list of anything!

# Adding
l2.append(2)
l2
```

## Lists {auto-animate=true}

```{python}
#| echo: true
l1 = [] # empty list
l2 = [1,2,3] # list of numbers
l3 = ['this','is','a','list'] # list of strings
l4 = ['WOW', 500, 3.4, l2] # list of anything!

# Removing
l2.pop(-1)
l2
```

## Dictionaries {auto-animate=true}

Dictionaries are **ordered**, **mutable** collections characterized by `key`:`value` pairs, which do not allow for **duplicates**.

```{python}
#| echo: true
d1 = {}
d2 = {
    1: 'something',
    2: "something else",
    'a': 500,
    'KEY': False,
    500: [True, None, 1, 'hello', 5e5]
     }
```

## Dictionaries {auto-animate=true}

```{python}
#| echo: true
d2 = {
    1: 'something',
    2: "something else",
     }
     
# Add a new entry
d2[3] = 'banana'
d2
```

## Dictionaries {auto-animate=true}

```{python}
#| echo: true
d2 = {
    1: 'something',
    2: "something else",
     }
     
# Modify an existing entry
d2[2] = 'banana'
d2
```

## Tuples {auto-animate=true}

Tuples are **ordered** and **immutable** data containers

```{python}
#| echo: true
t1 = (1,2,3)
t2 = ('this', 'is', 'a', 'tuple')
t3 = (None, True, 500, 'hola')
```

## Tuples {auto-animate=true}

```{python}
#| echo: true
t1 = (1,2,3)
t2 = ('this', 'is', 'a', 'tuple')
t3 = (None, True, 500, 'hola')

# Concatenation
t1 + t2
```

## Tuples {auto-animate=true}

```{python}
#| echo: true
t1 = (1,2,3)
t2 = ('this', 'is', 'a', 'tuple')
t3 = (None, True, 500, 'hola')

# Slicing
t3[1:3]
```

## Sets {auto-animate=true}

Sets are **unordered**, **unchangeable**, and **unindexed** data containers.

```{python}
#| echo: true
s1 = {1,2,3}
s2 = {'a','b','c'}
```

## Sets {auto-animate=true}

```{python}
#| echo: true
s3 = {1,2,2,2,2,4,5}

# Duplicates are removed
s3
```

## Conditionals {auto-animate=true}

Conditionals are used to do something only when one or multiple conditions are **fullfilled**.

```{python}
#| echo: true
number = 25

if number % 2 == 0:
    print(number, 'is even')
else: print(number, 'is odd')
```

## Conditionals {auto-animate=true}

```{python}
#| echo: true
number = 20

if number % 2 == 0:
    print(number, 'is even')
else: print(number, 'is odd')
```

## Conditionals {auto-animate=true}

```{python}
#| echo: true
#| code-line-numbers: "1,7"

number = 0

if number > 0:
    print(number, 'is positive')
elif number < 0:
    print(number, 'is negative')
else: print(number, 'is zero!')
```

## Conditionals {auto-animate=true}

```{python}
#| echo: true
#| code-line-numbers: "1,5,6"

number = -10

if number > 0:
    print(number, 'is positive')
elif number < 0:
    print(number, 'is negative')
else: print(number, 'is zero!')
```

## Loops {auto-animate=true}

**For loops** are extremely useful to run a block of code _many times_ by iterating over a **sequence**.

```{python}
#| echo: true

for num in range(5):
    print(num)
```

## Loops {auto-animate=true}

```{python}
#| echo: true

my_list = ['first element', 'second element', 'third element']
for element in my_list:
    print('This is the', element)
```

## Loops {auto-animate=true}

**While loops** can be used to repeat a block of code _until a certain condition_ is met.

```{python}
#| echo: true

my_string = "Oh no! I'm disappearing!"

while len(my_string) > 0:
    print(my_string)
    my_string = my_string[:-2]
```

# Python packages and functions

_Beyond the basics!_

## Functions
- Functions are wrappers for a block of code that you want to use **many times** or in **different situations**.
- Usually each function does a **single task**
- Python has a lot pre-defined functions
- Lots packages are available that contan functions for many applications
- You can define your own functions if you don't find one that you need!

## Functions {auto-animate=true}

Import a specific **function** from the math **package**

```{python}
#| echo: true
from math import floor

num = 10.2
floor(num)
```

## Functions {auto-animate=true}

Define your own **function**

```{python}
#| echo: true
def even_odd(number):
    if number % 2 == 0: print(number, 'is even')
    else: print(number, 'is odd')

num = 10
even_odd(num)
```

## Functions {auto-animate=true}

Define your own **function**

```{python}
#| echo: true
def even_odd(number):
    if number % 2 == 0: print(number, 'is even')
    else: print(number, 'is odd')

num = 33
even_odd(num)
```

## Functions to remember {auto-animate=true}
Print anything to console.

```{python}
#| echo: true
print("Hello World!")
```

## Functions to remember {auto-animate=true}
Get help documentation on anything.

```{python}
#| echo: true
import math

help(math.floor)
```

## Functions to remember {auto-animate=true}
See available attributes and methods of an object.

```{python}
#| echo: true
my_list = [1,2,3]
dir(my_list)
```

## Functions to remember {auto-animate=true}
Evaluate length of object.

```{python}
#| echo: true
my_list = [1,2,3]
print(my_list, 'has length:', len(my_list))

word = "potato"
print(word, 'has length:', len(word))
```

## Functions to remember {auto-animate=true}
Conversion of types.

```{python}
#| echo: true
print(int(5.2))
print(float(10))
print(bool(1))
print(list("abcd"))
print(set(["one","two"]))
print(dict([(3,"three"),(1,"one")]))
```

## Packages {auto-animate=true}

Import a whole **package**

$$ Area = \pi \cdot r^2 $$

```{python}
#| echo: true
import math

circle_radius = 2
area = math.pi * circle_radius ** 2
area
```

## Packages {auto-animate=true .smaller}

Import **function** to read and display an image from external **packages**.

External packages have to be installed before they can be imported.

```{python}
#| echo: true
from skimage.io import imread 
from matplotlib import pyplot as plt

logo = imread('./assets/igbmc.png')
plt.figure(figsize=(4,2))
plt.imshow(logo)
plt.axis('off')
plt.show()
```

# Tabular Data
![](https://pandas.pydata.org/static/img/pandas.svg)

## Create tabular data

```{python}
#| echo: true
import pandas as pd

data = {'Col 1': [3, 2, 1, 0], 'Col 2': ['a', 'b', 'c', 'd']}
pd.DataFrame.from_dict(data)
```

## Read tabular data

```{python}
#| echo: true
import pandas as pd
df = pd.read_csv('./assets/sample.csv')
df
```

## Explore tabular data {auto-animate=true}

```{python}
#| echo: true
df.info()
```

## Explore tabular data {auto-animate=true .smaller}

```{python}
#| echo: true
df.describe()
```

## Select data {auto-animate=true}
 Select single column

```{python}
#| echo: true
# Preferred
df['Food'] 

# Alternative
df.Food
```

## Select data {auto-animate=true}
Select 2 columns

```{python}
#| echo: true
df[['Food','Rating']]
```

## Select data {auto-animate=true}
Select row `1`

```{python}
#| echo: true
df.loc[1]
```

## Select data {auto-animate=true}
Select several rows

```{python}
#| echo: true
df.loc[1:3]
```

## Select data {auto-animate=true}
Select several rows and columns

```{python}
#| echo: true
df.loc[1:3, ['Food', 'Rating']]
```

## Select data {auto-animate=true}
Select based on a condition.

```{python}
#| echo: true
df[df['Rating'] > 3]
```

## Select data {auto-animate=true}
Select based on multiple conditions.

```{python}
#| echo: true
df[(df['Rating'] > 3) & (df['Food'].str.contains('Cheese'))]
```


## Create a new column {auto-animate=true}

Normalize rating between 0 and 1

```{python}
#| echo: true
df['Rating Norm'] = (df['Rating'] - 1) / (5 - 1)
df
```

## Group data {auto-animate=true}

```{python}
#| echo: true
df.groupby('Italian Category').mean()
```

# Create visualizations
![](https://matplotlib.org/stable/_images/sphx_glr_logos2_003.png)
![](https://seaborn.pydata.org/_images/logo-wide-lightbg.svg)

## Matplotlib

> _Matplotlib is a comprehensive library for creating static, animated, and interactive visualizations in Python._

> _Matplotlib makes easy things easy and hard things possible._

By far, the most common library used in scientific literature.

## Matplotlib structure
![[Source](https://matplotlib.org/stable/tutorials/introductory/quick_start.html)](https://matplotlib.org/stable/_images/anatomy.png){width=60%}

## Matplotlib structure {.smaller}

```{python}
import numpy as np
x = np.linspace(0, 2, 100)

import seaborn as sns
sns.reset_orig()
```

### Function-oriented style (simpler)
```{python}
#| echo: true
plt.figure(figsize=(5, 2.7))
plt.plot(x, x, label='linear')
plt.plot(x, x**2, label='quadratic')
plt.xlabel('X label')
plt.ylabel('Y label')
plt.title("Plot title")
plt.legend()
plt.show()
```

## Matplotlib structure {.smaller}
### Object-oriented style (more precise)

```{python}
#| echo: true
from matplotlib import pyplot as plt
fig, ax = plt.subplots(1, figsize=(5, 2.7))
ax.plot(x, x, label='linear')
ax.plot(x, x**2, label='quadratic')
ax.set_xlabel('X label')
ax.set_ylabel('Y label')
ax.set_title("Plot title")
ax.legend()
plt.show()
```

## Matplotlib plotting functions {.smaller}

`plt.plot` by default is a line plot, but it can modified with many parameters.

```{python}
#| echo: true
fig, ax = plt.subplots(1, figsize=(5, 2.7))
ax.plot(x, x)
ax.plot(x, 5+x**2)
plt.show()
```

## Matplotlib plotting functions {.smaller}

`plt.plot` by default is a line plot, but it can modified with many parameters.

```{python}
import numpy as np
x = np.linspace(0, 5, 20)
```

```{python}
#| echo: true
fig, ax = plt.subplots(1, figsize=(5, 2.7))
ax.plot(x, x, 'go--')
ax.plot(x, 5+x**2, marker='D', markersize=4, linestyle='', color='red')
plt.show()
```

## Matplotlib plotting functions {.smaller}

`plt.hist` creates a histogram of the data.

```{python}
import numpy as np
x = np.random.normal(0, 1, 500)
```

```{python}
#| echo: true
fig, ax = plt.subplots(1, figsize=(5, 2.7))
ax.hist(x, bins=20, rwidth=0.9)
plt.show()
```

## Matplotlib plotting functions {.smaller}

`plt.scatter` is for scatter plots.

It supports conditional formatting of the plot based on the data.

```{python}
import numpy as np
data = {'a': np.arange(50),
        'c': np.random.randint(0, 50, 50),
        'd': np.random.randn(50)}
data['b'] = data['a'] + 10 * np.random.randn(50)
data['d'] = np.abs(data['d']) * 100
```

```{python}
#| echo: true

fig, ax = plt.subplots(1, figsize=(5, 2.7))
ax.scatter(data['a'] , data['b'], c=data['c'], s=data['d'])
plt.show()
```

## Matplotlib plotting functions {.smaller}

`plt.axvline` and `plt.axhline` can be used to draw straight lines.

```{python}
import numpy as np
x = np.random.normal(0, 1, 500)
```

```{python}
#| echo: true

fig, axes = plt.subplots(1,2, figsize=(8, 2.7))
axes[0].hist(x, bins=20, rwidth=0.9)
axes[0].axvline(0, color='red', linewidth=2, linestyle='--')
axes[1].hist(x, bins=20, rwidth=0.9, orientation='horizontal')
axes[1].axhline(0, color='red', linewidth=2, linestyle='--')
plt.show()
```

## Matplotlib annotations {.smaller}

`plt.text` and `plt.annotate` allow for any type of annotations.

```{python}
import numpy as np
x = np.random.normal(0, 3, 500)
median = np.percentile(x, 50)
perc_25 = np.percentile(x, 25)
perc_75 = np.percentile(x, 75)
```

```{python}
#| echo: true

fig, ax = plt.subplots(1,1, figsize=(5, 3))
ax.hist(x, bins=20)
ax.axvline(median, color='red', linewidth=2, linestyle='--')
ax.axvline(perc_25, color='orange', linewidth=2, linestyle='--')
ax.axvline(perc_75, color='orange', linewidth=2, linestyle='--')
ax.set_ylim(0,80)
ax.text(0.85, 0.85, 'Norm. Distr.\n$\mu = 0$\n$\sigma = 3$', 
    va='center', ha='center', transform=ax.transAxes, bbox={'ec': 'black', 'fc': 'white'})
ax.annotate('Median', xy=(median, 70), xytext=(-6,60), 
    arrowprops=dict(arrowstyle="->", connectionstyle="arc3,rad=-0.4"))
plt.show()
```

## Matplotlib plotting functions {.smaller}

With `plt.subplots` we can create as many subpanel we want in the same figure.

```{python}
import numpy as np
x1 = np.random.normal(0,1,100)
x2 = np.random.normal(1,2,100)
x3 = np.random.uniform(0,255, (10,10))
```

```{python}
#| echo: true

fig, axes = plt.subplots(1,4, figsize=(10, 2.5), layout='constrained')
axes[0].plot(x1, x1**2, linestyle='', marker='.')
axes[1].scatter(x1,x2)
axes[2].hist(x2)
axes[2].hist(x1)
axes[3].imshow(x3)
plt.show()
```

## Save your figure {.smaller}

`plt.savefig` saves figures in many different formats

```{python}
import numpy as np
x1 = np.random.normal(0,1,100)
x2 = np.random.normal(1,2,100)
x3 = np.random.uniform(0,255, (10,10))
```

```{python}
#| echo: true

fig, axes = plt.subplots(1,4, figsize=(10, 2.5), layout='constrained')
axes[0].plot(x1, x1**2, linestyle='', marker='.')
axes[1].scatter(x1,x2)
axes[2].hist(x2)
axes[2].hist(x1)
axes[3].imshow(x3)
plt.savefig('./assets/my-figure.png', dpi=300) # for a raster image it's useful to specify resolution
plt.savefig('./assets/my-figure.svg')
plt.savefig('./assets/my-figure.pdf')
```

## Seaborn for grouped visualizations
> _Seaborn is a Python data visualization library based on Matplotlib_

> _It provides a high-level interface for drawing attractive and informative statistical graphics._

It allows for quicker beautiful visualization and it enables easy grouped visualization.

## Seaborn philosophy {auto-animate=true, .smaller}

```{python}
import pandas as pd
import seaborn as sns
df = sns.load_dataset('tips')
```

Every plotting function has a different name. I.e. `scatterplot`, `lineplot`, `boxplot`...

```{python}
#| echo: true

sns.lineplot(data=df, x='day',y='tip', hue='sex')
plt.show()
```

## Seaborn philosophy {auto-animate=true, .smaller}

```{python}
import pandas as pd
import seaborn as sns
df = sns.load_dataset('penguins')

```

Every plotting function has a different name. I.e. `scatterplot`, `lineplot`, `boxplot`...

```{python}
#| echo: true

sns.scatterplot(data=df, x='bill_length_mm', y='body_mass_g', hue='island')
plt.show()
```

## Seaborn philosophy {auto-animate=true, .smaller}

Every plotting function has a different name. I.e. `scatterplot`, `lineplot`, `boxplot`...

```{python}
#| echo: true

sns.boxplot(data=df, x='species', y='body_mass_g', hue='island')
plt.show()
```

## Seaborn philosophy {auto-animate=true, .smaller}

You can stack multiple plots together

```{python}
#| echo: true

sns.boxplot(data=df, x='species', y='body_mass_g', hue='sex')
sns.stripplot(data=df, x='species', y='body_mass_g', hue='sex', color='darkgray', dodge=True)
plt.show()
```

## Seaborn Figure-level functions {auto-animate=true, .smaller}

Seaborn has many functions to create entire figures instead of individual plots.

```{python}
#| echo: true
import seaborn as sns
sns.catplot(kind='violin', data=df, x='species', y='body_mass_g', col='sex', height=4)
plt.show()
```

## Seaborn Figure-level functions {auto-animate=true, .smaller}

Seaborn has many functions to create entire figures instead of individual plots.

```{python}
#| echo: true
import seaborn as sns
sns.lmplot(data=df, x='bill_length_mm', y='body_mass_g', col='island', row='sex', hue='species', height=2.5)
plt.show()
```

## Combine Seaborn with Matplotlib {.smaller}
```{python}
#| echo: true
fig, axes = plt.subplots(1,2, figsize=(10,4))
sns.scatterplot(data=df, x='bill_length_mm', y='body_mass_g', hue='species', ax=axes[0])
axes[0].set_title('Scatter plot')
sns.histplot(data=df, x='body_mass_g', kde=True, ax=axes[1])
axes[1].axvline(df['body_mass_g'].mean(), color='orange', linewidth=2, linestyle='--')
axes[1].set_title('Distribution')
plt.show()
```

## Seaborn context and style {auto-animate=true, .smaller}
```{python}
#| echo: true
sns.set_style('darkgrid')
sns.scatterplot(data=df, x='bill_length_mm', y='body_mass_g', hue='species')
plt.show()
```

## Seaborn context and style {auto-animate=true, .smaller}
```{python}
#| echo: true
sns.set_style('ticks')
sns.scatterplot(data=df, x='bill_length_mm', y='body_mass_g', hue='species')
plt.show()
```

## Seaborn context and style {auto-animate=true, .smaller}
```{python}
#| echo: true
sns.set_context('talk')
sns.scatterplot(data=df, x='bill_length_mm', y='body_mass_g', hue='species')
plt.show()
```

## Seaborn context and style {auto-animate=true, .smaller}
```{python}
#| echo: true
sns.set_context('paper')
sns.scatterplot(data=df, x='bill_length_mm', y='body_mass_g', hue='species')
plt.show()
```

## Seaborn context and style {auto-animate=true, .smaller}
```{python}
#| echo: true
sns.set(context='notebook', style='dark')
sns.scatterplot(data=df, x='bill_length_mm', y='body_mass_g', hue='species')
plt.show()
```

# It's a wrap!

## Additional materials {auto-animate=true}

There are many Python courses online:

- [CodeAcademy](https://www.codecademy.com/catalog/language/python)
- [Coursera](https://www.coursera.org/courses?query=python)
- [EdX](https://www.edx.org/learn/python)

## Additional materials {auto-animate=true}

Additional packages that can be on interest:

- Scikit-image &rarr; image processing
- Scikit-learn &rarr; all things machine learning
- TensorFlow and PyTorch &rarr; all things deep learning
- Scipy &rarr; statistic tests, signal processing, filters..

## Where to find help

- `help()` function in Python
- _Just Google it_
- [Stack Overflow](https://stackoverflow.com/)
- [Quora](https://quora.com/)
- Other's repositories on Github / Gitlab

# Thank you {.smaller}

:::: {.columns}
::: {.column  width="60"}
- Material: [Gitlab repository](https://gitlab.com/igbmc/mic-photon/python-intro-igbmc)
- Please fill in the [Feedback form](https://forms.gle/UQHk1qcrKf4MGVsbA)
- Contact me: [dallavem@igbmc.fr](mailto:dallavem@igbmc.fr)
:::

::: {.column  width="40"}
&#8595; Presentation Slides &#8595;
![](assets/qrcode_igbmc.gitlab.io.png)
:::
::::

::: footer
Introduction to Python programming for researchers @ IGBMC - Marco Dalla Vecchia
:::
