# Introduction to Python programming

This is material for the course "_Introduction to Python programming for researchers @ IGBMC_".

## Description Summary

This course is a programming with Python absolute beginner course. It is aimed at academic researchers, technicians and engineers at any level and especially at biologists who often are too scared of / don't have enough time for / think they cannot learn how to program. The truth is that everyone can have HUGE benefits from knowing basic and simple programming skills to automate, process and speed up (often boring and long) tasks. 

Keep in mind this will only cover the basics! Instead of teaching advanced data analysis or image processing concepts we will aim at learning the basic tools and mindset needed to start programming in your everyday tasks. From there, you should be able to discover and learn tools and techniques specific to your own needs and more advanced and specific courses could be organized in the future.

## Content

### Slides

Contains the slides for the course presentation. They are deployed via Gitlab pages [here](https://igbmc.gitlab.io/mic-photon/python-intro-igbmc/). These are more generally applicable and can be used to learn most basics Python concepts regardless of the hands-on project.

### Hands-on

Here you can find the data, notebooks and exercises for the course. These are focused on the hands-on project for the course.

### Course schedule

|   **Time**   | **Day** |  **Room** |
|:------------:|:-------:|:---------:|
|  9.30-12.00  |  03.05  | E1031 CBI |
|  13.30-17.00 |   03.05 | E1031 CBI |
|   9.30-12.00 |  10.05  |  3013 ICS |
|  13.30-17.00 |   10.05 |  3013 ICS |

## Acknowledgements

The course follows a simple example hands-on project to illustrate the applications of the concepts. All the data regarding this project are found in the `hands-on/data/` folder and have been kindly offered by [Riveline's Lab](https://www.igbmc.fr/igbmc/missions/annuaire/daniel-riveline) at IGBMC.

## Author

Marco Dalla Vecchia (dallavem@igbmc.fr)

## License

This course is under a Creative Commons license. You are free to re-use or re-distribute this material by citing its source. Please read the LICENSE details present in the repository for more details.